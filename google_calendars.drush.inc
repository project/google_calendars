<?php

/**
 * @file
 * {@inheritdoc}
 */

/**
 * Implements hook_drush_command().
 */
function google_calendars_drush_command() {
  return [
    'import-calendars' => [
      'description' => 'Import google calendars',
    ],
  ];
}

/**
 * {@inheritdoc}
 */
function drush_google_calendars_import_calendars() {
  $container = Drupal::getContainer();
  $service = $container->get("google_calendars.service");
  $service->sync();
}
