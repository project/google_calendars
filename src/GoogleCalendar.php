<?php

namespace Drupal\google_calendars;

use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactory;
use Google_Client;
use Google_Service_Calendar;
use PDO;

/**
 * {@inheritdoc}
 */
class GoogleCalendar {

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $db, ConfigFactory $config) {
    $this->db = $db;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public function sync() {
    $calendars = $this->getCalendars();
    foreach ($calendars as $calendar) {
      echo 'importing calendar' . $calendar['source_id'] . "\n";
      $this->importCalendar($calendar);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function importCalendar($calendar) {
    $config = $this->config->get("google_calendar.settings");
    $options = [
      'maxResults' => 100,
      'singleEvents' => TRUE,
      'timeZone' => 'America/Denver',
    ];
    if (!empty($calendar['token'])) {
      $options['syncToken'] = $calendar['token'];
    }
    else {
      $options["timeMin"] = date("c", strtotime($config->get("time_min")));
      $options["timeMax"] = date("c", strtotime($config->get("time_max")));
    }

    $pageToken = NULL;
    do {
      if ($pageToken != NULL) {
        $options["pageToken"] = $pageToken;
      }
      $results = $this->getService()->events->listEvents($calendar["source_id"], $options);
      foreach ($results->getItems() as $item) {
        $this->importEvent($calendar, $item);
      }
      $pageToken = $results->getNextPageToken();
    } while ($pageToken != NULL);

    $syncToken = $results->getNextSyncToken();
    $this->db->update("google_calendars")->fields([
      "token" => $syncToken,
      "imported" => time(),
    ])->condition("id", $calendar["id"])->execute();
  }

  /**
   * {@inheritdoc}
   */
  protected function importEvent($calendar, $event) {
    $start = $event->getStart();
    $end = $event->getEnd();
    if (empty($start)) {
      return;
    }
    $record = [
      "google_calendars_id" => $calendar["id"],
      "source_id" => $event->getId(),
      "summary" => $event->getSummary(),
      "description" => $event->getDescription() ?: '',
      "location" => $event->getLocation() ?: '',
      "start" => $start->getDateTime() ? strtotime($start->getDateTime()) - 21600 : strtotime($start->getDate()),
      "end" => $end->getDateTime() ? strtotime($end->getDateTime()) - 21600 : strtotime($end->getDate()),
      "url" => $event->getHtmlLink(),
      "created" => strtotime($event->getCreated()),
      "modified" => strtotime($event->getUpdated()),
      "imported" => time(),
    ];

    $exists = $this->db->select("google_events", "e")->fields("e")
      ->condition("e.google_calendars_id", $calendar["id"])
      ->condition("e.source_id", $event->getId())
      ->execute()->fetchAll(PDO::FETCH_ASSOC);
    if (empty($exists)) {
      $this->db->insert("google_events")->fields($record)->execute();
    }
    else {
      $this->db->update("google_events")->fields($record)->condition("id", $exists[0]["id"])->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getService() {
    if ($this->service == NULL) {
      $config = $this->config->get("google_calendar.settings");
      $client = new Google_Client();
      $client->setApplicationName($config->get("application_name"));
      $client->setDeveloperKey($config->get("api_key"));
      $this->service = new Google_Service_Calendar($client);
    }
    return $this->service;
  }

  /**
   * {@inheritdoc}
   */
  public function getCalendars() {
    return $this->db->select("google_calendars", "c")->fields('c')->execute()->fetchAll(PDO::FETCH_ASSOC);
  }

}
