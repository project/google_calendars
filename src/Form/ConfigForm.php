<?php

namespace Drupal\google_calendars\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Google Calendar module configuration.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_calendar_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'google_calendar.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('google_calendar.settings');

    $form['application_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Application Name'),
      '#default_value' => $config->get('application_name'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
    ];

    $form['time_min'] = [
      '#type' => 'date',
      '#title' => $this->t('Date to start importing from'),
      '#default_value' => $config->get('time_min'),
    ];

    $form['time_max'] = [
      '#type' => 'date',
      '#title' => $this->t('Date to stop importing'),
      '#default_value' => $config->get('time_max'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config('google_calendar.settings')
      ->set('application_name', $form_state->getValue('application_name'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('time_min', $form_state->getValue('time_min'))
      ->set('time_max', $form_state->getValue('time_max'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
