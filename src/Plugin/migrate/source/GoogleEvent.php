<?php

namespace Drupal\google_calendars\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\Core\Extension\ModuleHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Source plugin for google events.
 *
 * @MigrateSource(
 *   id = "google_event"
 * )
 */
class GoogleEvent extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, ModuleHandler $modules) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state);
    $this->modules = $modules;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get("module_handler")
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    /*
     * An important point to note is that your query *must* return a single row
     * for each item to be imported. Here we might be tempted to add a join to
     * migrate_example_beer_topic_node in our query, to pull in the
     * relationships to our categories. Doing this would cause the query to
     * return multiple rows for a given node, once per related value, thus
     * processing the same node multiple times, each time with only one of the
     * multiple values that should be imported. To avoid that, we simply query
     * the base node data here, and pull in the relationships in prepareRow()
     * below.
     */
    $query = $this->select('google_events', 'e')->fields('e');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'id' => $this->t('Record ID'),
      'google_calendars_id' => $this->t('Calendar record ID'),
      'source_id' => $this->t('Unique ID from Google'),
      'summary' => $this->t('Summary/Title of the event'),
      'description' => $this->t('Longer event description'),
      'location' => $this->t('Location of event'),
      'start' => $this->t('Start date/time'),
      'end' => $this->t('End date/time'),
      'url' => $this->t('URL to event'),
      'created' => $this->t('Record creation time'),
      'modified' => $this->t('Last modified time of the record'),
      'imported' => $this->t('Last import time of the record'),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
        'alias' => 'e',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    foreach (["start", "end"] as $column) {
      $row->setSourceProperty($column, date("Y-m-d\TH:i:s", $row->getSourceProperty($column)));
    }
    if ($this->modules->moduleExists("tac_calendars")) {
      $cid = $row->getSourceProperty("google_calendars_id");
      $query = $this->select("tac_calendars_access", 'c');
      $query->join("taxonomy_term_data", "t", "t.tid=c.tid");
      $query->condition('c.google_calendars_id', $cid);
      $query->fields("c");
      $query->fields("t", ["vid"]);
      $record = $query->execute()->fetchAssoc();
      if ($record) {
        $row->setSourceProperty($record["vid"], $record["tid"]);
      }
    }
    return parent::prepareRow($row);
  }

}
